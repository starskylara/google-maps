### `Ejercicio 1 - Teorico `

## Preguntas

**1. Que problemas detectas en la operación y razona la respuesta:?**

> Yo no diría problemas, diría más perspectiva. La intención es dividir la funcionalidad creando funciones que se encarguen de una sola cosa, haciendo que sean responsable de una sola funcionalidad. Dicho esto por eso propongo crear dos nuevos métodos que internamente verifiquen y obtengan el contenido necesario para hacer el calculo (getTotalPriceByContentType y getTotalPriceByContentType). De esta manera si en el futuro se decide agregar un nuevo tipo de contenido o servicio, de esta forma el código se puede actualizar sin tener que rediseñar la lógica que actualmente está operativa

> Lo que yo pondría seria que la función **getTotalPriceByContentType()** se encargara de iterar por los servicios, y que calculara el resultado aparir de dos funciones. La primera llamada **getTotalPriceByContentType()** se encargaría de obtener el precio a pagar diferenciando el tipo de contenido (StreamingService || DownloadService). La segunda función llamada **getAdditionalFee()** se encargaría de obtener el precio por gastos premium. Con estos dos gatos se puede obtener un total por servicio.
> **2. Propón una solución alternativa (también en pseudocódigo del mismo estilo) que corrija los problemas de la operación getTotal de RegisteredUser que has detectado en la pregunta anterior. Realiza todos los cambios que consideres necesarios en cualquiera de las clases del modelo del enunciado.**

> **Equema**
![Esquema](./src/assets/image.PNG)

---

**LA REPUESTA TAMBIEN ESTA DENTRO DE LOS COMENTARIOS DEL CODIGO**

```
class RegisteredUser {
  constructor(services = []) {
    super();
    this.services = services;
  }

  // esta funcion solo se encararia de optener el total
  // de UNO SOLO de los servicios
  // crearía una función
  // que se encargara solamente de
  // retornar los totales a partir del tipo de cargo que se hace
  // de esta manera si hay algún nuevo servicio.
  // la función que cambiaría/actualizaría sería estas y no modificaría la lógica de otros métodos
  getTotalPriceByContentType(service) {
    switch (service) {
      case StreamingService:
        return service.streamingPrice;
      case DownloadService:
        return service.downloadPrice;
      default:
        return 0;
    }
  }

  getAdditionalFee(multimediaContent) {
    // primero obtener el objeto/clase que contiene la información. este por lo entendio se encunetra dentro del objeto multimediaContent
    // del contenido premiun
    let premiumContent = multimediaContent.getPremiumContent();
    // si el contenido premium efectivamente existe ejecutar el retorno del mismo
    // o en su defecto retornar el 0 (esto de retornar zero no me gusta, pero por temas de ejercicio es lo más fácil para probar)
    if (premiumContent === typeof PremiumContent) {
      return premiumContent.additionalFee;
    }
    return 0;
  }

  // esta función solo se encararía de obtener el total
  // de TODOS los servicios
  getTotal() {
    let total = 0;
    this.services.forEach((service) => {
      let multimediaContent = service.getMultimediaContent();

      // función que se encarga de obtener el precio por typo de contenido
      priceByContentType = getTotalPriceByContentType(multimediaContent);
      priceAdditionalFeed = getAdditionalFee(multimediaContent);
      // el total es la suma del tipo de contenido + los costos adicionales
      total += (priceByContentType + priceAdditionalFeed);
    });
    return total;
  }
}

```
---
### `Ejercicio 2 - React -npm start`

```
    "start": "react-scripts start start",
    "start-map": "REACT_APP_API_GOOGLE=AIzaSyAgjJQZRDO7bpgtcKk9VBosMUHi3znEMqE react-scripts start",
    "start-map:win": "set REACT_APP_API_GOOGLE=AIzaSyAgjJQZRDO7bpgtcKk9VBosMUHi3znEMqE && react-scripts start",
```
