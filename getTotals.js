class RegisteredUser {
  constructor(services = []) {
    super();
    this.services = services;
  }

  // esta funcion solo se encararia de optener el total
  // de UNO SOLO de los servicios
  // crearía una función
  // que se encargara solamente de
  // retornar los totales a partir del tipo de cargo que se hace
  // de esta manera si hay algún nuevo servicio.
  // la función que cambiaría/actualizaría sería estas y no modificaría la lógica de otros métodos
  getTotalPriceByContentType(service) {
    switch (service) {
      case StreamingService:
        return service.streamingPrice;
      case DownloadService:
        return service.downloadPrice;
      default:
        return 0;
    }
  }

  getAdditionalFee(multimediaContent) {
    // primero obtener el objeto/clase que contiene la información
    // del contenido premiun
    let premiumContent = multimediaContent.getPremiumContent();
    // si el contenido premium efectivamente existe ejecutar el retorno del mismo
    // o en su defecto retornar el 0 (esto de retornar zero no me gusta, pero por temas de ejercicio es lo más fácil para probar)
    if (premiumContent === typeof PremiumContent) {
      return premiumContent.additionalFee;
    }
    return 0;
  }

  // esta función solo se encararía de obtener el total
  // de TODOS los servicios
  getTotal() {
    let total = 0;
    this.services.forEach((service) => {
      let multimediaContent = service.getMultimediaContent();

      // función que se encarga de obtener el precio por typo de contenido
      priceByContentType = getTotalPriceByContentType(multimediaContent);
      priceAdditionalFeed = getAdditionalFee(multimediaContent);
      // el total es la suma del tipo de contenido + los costos adicionales
      total += (priceByContentType + priceAdditionalFeed);
    });
    return total;
  }
}
