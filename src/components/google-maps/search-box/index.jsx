import React, { useEffect, useRef, useState } from "react";
import "./index.css";
import PropTypes from "prop-types";
/* global google */

const SearchBox = ({ onSetPlace }) => {
  const ref = useRef();
  const [autocomplete, setAutocomplete] = useState(null);

  useEffect(() => {
    setAutocomplete(
      new google.maps.places.Autocomplete(ref.current, {
        types: ["geocode"],
      })
    );
  }, []);

  useEffect(() => {
    if (autocomplete) autocomplete.addListener("place_changed", onPlaceChanged);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [autocomplete]);

  const onPlaceChanged = () => {
    const place = autocomplete.getPlace();
    onSetPlace(place);
  };

  return (
    <div className="searchBox">
      <input
        ref={ref}
        id="autocomplete"
        type="text"
        onFocus={() => (ref.current.value = "")}
        placeholder="Buscar direccion"
        style={{ height: "1rem", width: "100%", padding: "1rem" }}
      ></input>
    </div>
  );
};

SearchBox.propTypes = {
  onSetPlace: PropTypes.func,
};
SearchBox.defaultProps = {
  onSetPlace: () => null,
};

export default SearchBox;
