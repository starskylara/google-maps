import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import GoogleMapsLogo from "../assets/google-maps-logo";
import { actionAddMarker } from "../../infrastructure/redux/actions/googleMaps";
// import PropTypes from "prop-types";
import "./index.css";
import SearchBox from "./search-box";

const GoogleMaps = () => {
  const dispatch = useDispatch();
  const [api, setApi] = useState(null);
  const [map, setMap] = useState(null);
  const [marker, setNewMarker] = useState(null);

  useEffect(() => {
    if (process.env.REACT_APP_API_GOOGLE) {
      setApi(process.env.REACT_APP_API_GOOGLE);
    }
  }, []);

  useEffect(() => {
    if (marker) dispatch(actionAddMarker(marker));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [marker]);

  useEffect(() => {
    if (api) {
      if (window.google) {
        onLoadMap();
      } else {
        const map = document.getElementById("scriptGoogleMaps");
        map.type = "text/javascript";
        map.src = `https://maps.google.com/maps/api/js?key=${api}`;
        const nodeMap = document.getElementById("GoogleMaps");
        nodeMap.parentNode.insertBefore(map, nodeMap);
        map.addEventListener("load", () => {
          onLoadMap();
        });
      }
    }
  }, [api]);

  const onSetPlace = (place) => {
    if (map && place.geometry && place.geometry.location) {
      map.setCenter(place.geometry.location);
      const markerConfig = {
        position: place.geometry.location,
        map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
          labelOrigin: new window.google.maps.Point(90, 42),
          size: new window.google.maps.Size(32, 32),
          anchor: new window.google.maps.Point(16, 32),
        },
        label: {
          text: place.formatted_address,
          color: "#C70E20",
          fontWeight: "bold",
          fontSize: "18px",
        },
      };
      new window.google.maps.Marker(markerConfig); // solo he implementado barrio para especificos zooms.
      // debo investigar mas como podria ser una forma mas optima documentacion https://developers.google.com/maps/documentation/places/web-service/supported_types
      setNewMarker(markerConfig);
      if (place.types && place.types[0] === "neighborhood") map.setZoom(16);
      else map.setZoom(13);
    }
  };

  const onLoadMap = () => {
    const mapId = document.getElementById("GoogleMaps");
    setMap(
      new window.google.maps.Map(mapId, {
        center: { lat: 41.3785439, lng: 2.1708129 },
        zoom: 13,
      })
    );
  };

  return (
    <>
      {api ? (
        <>
          <SearchBox onSetPlace={onSetPlace} />
          <div id="GoogleMaps"></div>
        </>
      ) : (
        <div id="GoogleMaps">
          <div className="header">
            <h2>Intentalo de nuevo</h2>
          </div>
          <div className="logo">
            <GoogleMapsLogo />
          </div>
          <div className="message">
            <h2>Es probable que has iniciando la aplicacion con "npm start"</h2>
            <p>
              si estas en un Mac intenta con "npm run start-map". <br />
              si estas en un Win intenta con "start-map:win"
            </p>
            <p>
              Para mas detalles. puedes ver los scripts dentro el package.json"
            </p>
          </div>
        </div>
      )}
    </>
  );
};

GoogleMaps.propTypes = {};

export default GoogleMaps;
