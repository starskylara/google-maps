import React from "react";
// import PropTypes from 'prop-types'
import Logo from "./google-maps.png";

const GoogleMapsLogo = () => {
  return <img src={Logo} alt="Logo" />;
};

GoogleMapsLogo.propTypes = {};

export default GoogleMapsLogo;
