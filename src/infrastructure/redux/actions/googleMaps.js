import { ADD_NEW_MARKER } from "../constants/googleMaps";

export const actionAddMarker = (marker = {}) => ({
  type: ADD_NEW_MARKER,
  marker,
});
