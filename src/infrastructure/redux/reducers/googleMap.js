import { ADD_NEW_MARKER } from "../constants/googleMaps";

const initialState = {
  marker: [],
};

const mapsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_MARKER:
      return {
        ...state,
        marker: [...state.marker, action.marker],
      };
    default:
      return state;
  }
};

export default mapsReducer;
