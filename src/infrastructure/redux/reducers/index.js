import { combineReducers } from "redux";
import mapsReducer from "./googleMap";

export default combineReducers({
  mapsReducer,
});
