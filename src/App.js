// import "./App.css";
import GoogleMaps from "./components/google-maps";
import { Provider } from "react-redux";
import { store } from "./infrastructure/redux/store";

function App() {
  return (
    <Provider store={store}>
      <div id="App" className="App">
        <GoogleMaps />
      </div>
    </Provider>
  );
}

export default App;
